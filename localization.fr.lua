if GetLocale() ~= "frFR" then return end
local _, BTM = ...
local L = BTM.L

-- ==========| ROLL PATTERN |========== --
L.ROLL_PATTERN = "(.+) obtient un (%d+) %((%d+)%-(%d+)%)"

-- ==========| OPTIONS FRAME |========== --
L["Sort rolls ascending"]             = "Ordre croissant des scores"
L["Ignore rolls after countdown"]     = "Tout ignorer après le compte à rebours"
L["Use raid warnings"]                = "Utiliser les avertissements de raid"
L["Announce item winner"]             = "Annoncer le gagnant"
L["Announce held items"]              = "Annoncer les items gardés"
L["Announce reserved items"]          = "Annoncer les items réservés"
L["Announce disenchanted items"]      = "Annoncer les items désenchantés"
L["ScreenShot reminder before trade"] = "Rappel de capture d'écran avant tout échange"
L["Show items tooltips"]              = "Afficher les infobulles"
L["Show minimap button"]              = "Afficher le bouton de la mini-carte"
L["Debug mode"]                       = "Mode débogage"
L["Countdown Duration"]               = "Durée du c. à rebours"
L["Defaults"]                         = "Par défault"
L["Close"]                            = "Fermer"
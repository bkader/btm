local _, BTM = ...
local L = setmetatable({}, {__index = function(L, k)
	local v = tostring(k)
	rawset(L, k, v)
	return v
end})
BTM.L = L

-- ==========| ROLL PATTERN |========== --
L.ROLL_PATTERN = "(.+) rolls (%d+) %((%d+)%-(%d+)%)"

-- ==========| OPTIONS FRAME |========== --
-- L["Sort rolls ascending"]             = "Sort rolls ascending"
-- L["Ignore rolls after countdown"]     = "Ignore rolls after countdown"
-- L["Use raid warnings"]                = "Use raid warnings"
-- L["Announce item winner"]             = "Announce item winner"
-- L["Announce held items"]              = "Announce held items"
-- L["Announce reserved items"]          = "Announce reserved items"
-- L["Announce disenchanted items"]      = "Announce disenchanted items"
-- L["ScreenShot reminder before trade"] = "ScreenShot reminder before trade"
-- L["Show items tooltips"]              = "Show items tooltips"
-- L["Show minimap button"]              = "Show minimap button"
-- L["Debug mode"]                       = "Debug mode"
-- L["Countdown Duration"]               = "Countdown Duration"
-- L["Defaults"]                         = "Defaults"
-- L["Close"]                            = "Close"
local addonName, BTM = ...
local L = BTM.L
_G["BTM"] = BTM

BTM.frameName = nil
BTM.UIFrame = nil
local locale = GetLocale()
local addonLoaded = false
local mFrame, pFrame, oFrame, lFrame

--------------------
-- Options Locals --
--------------------
local options = {}
local loadOptions
local editOptions = false
local defaultOptions = {
	sortAscending = false,
	countdownRollsBlock = true,
	useRaidWarning = true,
	announceOnWin = true,
	announceOnHold = true,
	announceOnBank = false,
	announceOnDisenchant = false,
	screenReminder = true,
	showTooltips = true,
	minimapButton = true,
	debugMode = false,
	countdownDuration = 5,
}

-- ========================================================= --
-- --------------------| OPTIONS FRAME |-------------------- --
-- ========================================================= --
do
	local frameName, UIFrame
	local localizeUIFrame, updateUIFrame, loadDefaultOptions
	local localized = false

	-- Complete missing options:
	local function addDefaultOptions(t1, t2)
		for i, v in pairs(t2) do
			if t1[i] == nil then
				t1[i] = v
			elseif type(v) == "table" then
				addDefaultOptions(v, t2[i])
			end
		end
	end

	-- Load addOn options:
	function loadOptions()
		options = BTM_SavedOptions
		addDefaultOptions(options, defaultOptions)
	end

	------------
	-- OnLoad --
	------------
	function BTM:OnLoadOptions(frame)
		if frame == nil then return end
		UIFrame = frame
		frameName = UIFrame:GetName()
		UIFrame:RegisterForDrag("LeftButton")
		UIFrame:SetClampedToScreen(true)
		UIFrame:SetScript("OnUpdate", updateUIFrame)
		UIFrame:SetScript("OnShow", function() editOptions = true end)
		UIFrame:SetScript("OnHide", function() editOptions = false end)
		localizeUIFrame()
		oFrame = UIFrame
	end

	-------------
	-- OnClick --
	-------------
	function BTM:OnClickOption(button)
		if button == nil then return end
		local value
		local name = button:GetName()
		if name == frameName.."_countdownDuration" then
			value = button:GetValue()
			_G[frameName.."_countdownDurationCurrent"]:SetText(value)
		else
			value = (button:GetChecked() == 1) or false
			if name == frameName.."_minimapButton" then
				self:ToggleMinimapButton()
			end
		end
		BTM_SavedOptions[strsub(name, strlen(frameName) + 2)] = value
	end

	--------------
	-- OnUpdate --
	--------------
	function localizeUIFrame()
		if localized or UIFrame == nil then return end
		if locale ~= "enUS" and locale ~= "enGB" then
			_G[frameName.."_sortAscendingString"]:SetText(L["Sort rolls ascending"])
			_G[frameName.."_countdownRollsBlockString"]:SetText(L["Ignore rolls after countdown"])
			_G[frameName.."_useRaidWarningString"]:SetText(L["Use raid warnings"])
			_G[frameName.."_announceOnWinString"]:SetText(L["Announce item winner"])
			_G[frameName.."_announceOnHoldString"]:SetText(L["Announce held items"])
			_G[frameName.."_announceOnBankString"]:SetText(L["Announce reserved items"])
			_G[frameName.."_announceOnDisenchantString"]:SetText(L["Announce disenchanted items"])
			_G[frameName.."_screenReminderString"]:SetText(L["ScreenShot reminder before trade"])
			_G[frameName.."_showTooltipsString"]:SetText(L["Show items tooltips"])
			_G[frameName.."_minimapButtonString"]:SetText(L["Show minimap button"])
			_G[frameName.."_debugModeString"]:SetText(L["Debug mode"])
			_G[frameName.."_countdownDurationString"]:SetText(L["Countdown Duration"])
			_G[frameName.."_DefaultsBtn"]:SetText(L["Defaults"])
			_G[frameName.."_CloseBtn"]:SetText(L["Close"])
		end
		_G[frameName.."_DefaultsBtn"]:SetScript("OnClick", loadDefaultOptions)
		_G[frameName.."_CloseBtn"]:SetScript("OnClick", function() UIFrame:Hide() end)
		localized = true
	end

	-----------------------
	-- Localize UI Frame --
	-----------------------
	function updateUIFrame()
		_G[frameName.."_sortAscending"]:SetChecked(BTM_SavedOptions.sortAscending == true)
		_G[frameName.."_countdownRollsBlock"]:SetChecked(BTM_SavedOptions.countdownRollsBlock == true)
		_G[frameName.."_useRaidWarning"]:SetChecked(BTM_SavedOptions.useRaidWarning == true)
		_G[frameName.."_announceOnWin"]:SetChecked(BTM_SavedOptions.announceOnWin == true)
		_G[frameName.."_announceOnHold"]:SetChecked(BTM_SavedOptions.announceOnHold == true)
		_G[frameName.."_announceOnBank"]:SetChecked(BTM_SavedOptions.announceOnBank == true)
		_G[frameName.."_announceOnDisenchant"]:SetChecked(BTM_SavedOptions.announceOnDisenchant == true)
		_G[frameName.."_screenReminder"]:SetChecked(BTM_SavedOptions.screenReminder == true)
		_G[frameName.."_showTooltips"]:SetChecked(BTM_SavedOptions.showTooltips == true)
		_G[frameName.."_minimapButton"]:SetChecked(BTM_SavedOptions.minimapButton == true)
		_G[frameName.."_debugMode"]:SetChecked(BTM_SavedOptions.debugMode == true)
		_G[frameName.."_countdownDuration"]:SetValue(BTM_SavedOptions.countdownDuration)
		_G[frameName.."_countdownDurationCurrent"]:SetText(BTM_SavedOptions.countdownDuration)
	end

	---------------------
	-- Default Options --
	---------------------
	function loadDefaultOptions()
		for k, v in pairs(defaultOptions) do
			BTM_SavedOptions[k] = v
		end
	end
end
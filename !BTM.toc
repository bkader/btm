## Interface: 30300
## Title: !|caa42f49eBTM|r|caaf49141asterLooter|r
## Title-zhCN: !|caa42f49eBTM|r|caaf49141asterLooter|r
## Title-deDE: !|caa42f49eBTM|r|caaf49141asterLooter|r
## Title-esES: !|caa42f49eBTM|r|caaf49141asterLooter|r
## Title-esMX: !|caa42f49eBTM|r|caaf49141asterLooter|r
## Title-frFR: !|caa42f49eBTM|r|caaf49141asterLooter|r
## Title-ruRU: !|caa42f49eBTM|r|caaf49141asterLooter|r
## Title-koKR: !|caa42f49eBTM|r|caaf49141asterLooter|r
## Title-zhTW: !|caa42f49eBTM|r|caaf49141asterLooter|r
## Notes: |caa42f49eBlame|r|caaf49141The|r|caa42f49eMaster|r|caaf49141Looter|r makes the loot master's life easier. Honorable Mention: |caa69ccf0BlameTheMage|r guild on |caa996019Warmane|r.
## SavedVariables: BTM_SavedOptions, BTM_ExportString
## SavedVariablesPerCharacter: BTM_Log
## LoadOnDemand: 0
## DefaultState: enabled
## Author: Kader Bouyakoub
localization.en.lua
localization.cn.lua
localization.de.lua
localization.es.lua
localization.fr.lua
localization.kr.lua
localization.ru.lua
localization.tw.lua
BTM.lua
BTM.xml
if GetLocale() ~= "esES" and GetLocale() ~= "esMX" then return end
local _, BTM = ...
local L = BTM.L

-- ==========| ROLL PATTERN |========== --
L.ROLL_PATTERN = "(.+) tira los dados y obtiene (%d+) %((%d+)%-(%d+)%)"
if GetLocale() ~= "ruRU" then return end
local _, BTM = ...
local L = BTM.L

-- ==========| ROLL PATTERN |========== --
L.ROLL_PATTERN = "(.+) rolls (%d+) %((%d+)%-(%d+)%)"
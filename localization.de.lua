if GetLocale() ~= "deDE" then return end
local _, BTM = ...
local L = BTM.L

-- ==========| ROLL PATTERN |========== --
L.ROLL_PATTERN = "(.+) würfelt. Ergebnis: (%d+) %((%d+)%-(%d+)%)"